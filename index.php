
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pop Group</title>

    <!-- Bootstrap core CSS-->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" type="text/css" >
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato:300,400,700'>

  

  </head>


<body>

<!-- Just an image -->
<nav class="navbar navbar-light bg-dark fixed-top">
  <a class="navbar-brand" href="#" style="color: orange">
    <img src="images/popLogo.png" width="60" height="30" alt="">
    
  </a>

  <ul class="nav justify-content-end">
  <li class="nav-item" >
    <a class="nav-link active" style="color:orange" href="index.php">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" style="color:orange" href="#service">Services</a>
  </li>

  <li class="nav-item" >
    <a class="nav-link" style="color:orange" href="#about">About Us</a>
  </li>
  
  <li class="nav-item">
    <a class="nav-link" style="color:orange" href="#contact" >Contact</a>
  </li>
</ul>
</nav>


<div class="jumbotron"  >
	<div id="head">
      <h2 class="display-4" >PRODUCTIVITY </h2>
      <h2 class="display-4" > OUT PRICE</h2>
	</div>	  
</div>
<br>
				  
	<div class="container-fluid space" id="service">
			<div class="col-xl-6 mx-auto text-center">
                  <div class="section-title mb-100">
                     <p>Get To Know</p>
                     <h4>Our Services</h4>
                  </div>
               </div>
		<div class="row">
		<div class="col col-lg-4 col-md-8 mx-auto">
			<div class="card">
		  	<img src="images/mediah.png" class="card-img-top" alt="...">
		  	<div class="card-body">
		    <h5 class="card-title">Pop Media</h5>
		    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
		    <a href="#" class="btn btn-dark " style="color:orange">More Information</a>
		  	</div>
			</div>
		</div>
		
		<div class="col-lg-4 col-md-8 mx-auto">
			<div class="card">
		  	<img src="images/development.png" class="card-img-top" alt="...">
		  	<div class="card-body">
		    <h5 class="card-title">Pop Developpement</h5>
		    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
		    <a href="#" class="btn btn-dark " style="color:orange">More Information</a>
		  	</div>
			</div>
		</div>


		<div class="col col-lg-4 col-md-8 mx-auto">
			<div class="card">
		  	<img src="images/meeting.png" class="card-img-top" alt="...">
		  	<div class="card-body">
		    <h5 class="card-title">Pop Services</h5>
		    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
		    <a href="#" class="btn btn-dark " style="color:orange">More Information</a>
		  	</div>
			</div>
		</div>	
	</div>	

</div>


<div class="jumbotron" id="about">
  	<section id="team" class="pb-5">
    <div class="container-fluid">
				  <div class="section-title mb-100">
                     <p>You Should Meet</p>
                     <h4>Our Team</h4>
                  </div>
        <div class="row">
            <!-- Team member -->
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <p><img class=" img-fluid" src="images/user.jpg" alt="card image"></p>
                                    <h4 class="card-title">Bertholin AMBROISE</h4>
                                    <p class="card-text">This is basic card with image on top, title, description and button.</p>
                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">Bertholin AMBROISE</h4>
                                    <p class="card-text">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-skype"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-google"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->
            <!-- Team member -->
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <p><img class=" img-fluid" src="https://sunlimetech.com/portfolio/boot4menu/assets/imgs/team/img_02.png" alt="card image"></p>
                                    <h4 class="card-title">Kindy PHILLIBERT</h4>
                                    <p class="card-text">This is basic card with image on top, title, description and button.</p>
                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">Kindy PHILLIBERT</h4>
                                    <p class="card-text">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-skype"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-google"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->
            <!-- Team member -->
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <p><img class=" img-fluid" src="images/mac.jpg" alt="card image"></p>
                                    <h4 class="card-title">Mac Henry MENTOR</h4>
                                    <p class="card-text">This is basic card with image on top, title, description and button.</p>
                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">Mac Henry MENTOR</h4>
                                    <p class="card-text">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-skype"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-google"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->
           
        </div>
    </div>
</section>
<!-- Team -->
</div>
	
 <section class="contact pt-100 pb-100" id="contact">
         <div class="container">
            <div class="row">
               <div class="col-xl-6 mx-auto text-center">
                  <div class="section-title mb-100">
                     <p>We Would Like to Get In Touch</p>
                     <h4>Contact Us</h4>
                  </div>
               </div>
            </div>
            <div class="row text-center">
                  <div class="col-md-8">
                     <form action="#" class="contact-form">
                        <div class="row">
                           <div class="col-xl-6">
                              <input type="text" placeholder="name">
                           </div>
                           <div class="col-xl-6">
                              <input type="text" placeholder="email">
                           </div>
                           <div class="col-xl-6">
                              <input type="text" placeholder="subject">
                           </div>
                           <div class="col-xl-6">
                              <input type="text" placeholder="telephone">
                           </div>
                           <div class="col-xl-12">
                              <textarea placeholder="message" cols="30" rows="10"></textarea>
                              <input type="submit" value="send message">
                           </div>
                        </div>
                     </form>
                  </div>
                  <div class="col-md-4">
                     <div class="single-contact">
                        <i class="fa fa-map-marker"></i>
                        <h5>Address</h5>
                        <p>18 DELMAS, Port-au-Prince, Haiti</p>
                     </div>
                     <div class="single-contact">
                        <i class="fa fa-phone"></i>
                        <h5>Phone</h5>
                        <p>(+509) 3185 7940</p>
                     </div>
                     <div class="single-contact">
                        <i class="fa fa-envelope"></i>
                        <h5>Email</h5>
                        <p>infor@personal.com</p>
                     </div>
                  </div>
            </div>
         </div>
      </section>
	






<div class="footer" >
	<nav class="navbar bottom navbar-light bg-dark">
		<p id="footer" align="center"><a href="popgroupayiti.com">&copy POPGROUPAYITI.COM 2019</a></p>

	</nav>
</div>


</body>
</html>